package uni.fiis.poo.pc2;

public class Libros extends MaterialUniversitario implements Estudiable {
    private String codigo;
    private static String titulo;
    private String autor;
    private int num_pag;
    private int anio;
    private String descripcion;

    public Libros(String curso,String universidad, int ciclo, double precio, String codigo, String titulo, String autor, int num_pag, int anio, String descripcion){
        super(curso, universidad, ciclo, precio);
        this.codigo=codigo;
        this.titulo=titulo;
        this.autor=autor;
        this.num_pag=num_pag;
        this.anio=anio;
    }

    public static String getTitulo() {
        return titulo;
    }
    public void mostarDatos(){
        System.out.println("Libro");
        System.out.println("Codigo: "+codigo);
        System.out.println("Titulo: "+titulo);
        System.out.println("Autor: "+autor);
        System.out.println("Año de publicacion: "+anio);
        System.out.println("Numero de paginas: "+num_pag);
    }

    @Override
    public int obtenerTiempoEstudio() {
        int tiempo=num_pag*5;
        return tiempo;
    }
}
