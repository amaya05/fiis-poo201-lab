package uni.fiis.poo.pc2;

public class Main {

    public static void main(String[] args) {
        Libros libros1= new Libros("Fisica 1","UNI",3, 30,"CB102","Fisica para todos","Alvarez",200,2015," ");
        Libros libros2= new Libros("Fisica 1","UTEC",3, 50,"CV210","Fisica 1","Morales",300,2017," ");
        Películas películas1= new Películas("Calculo 2","UNSA",2,10,"Calculo 2","ciencia",90," ");
        Películas películas2= new Películas("Calculo 2","UNI",2,10,"Calculo integral","ciencia",60," ");
        Documentales documentales1= new Documentales("Quimica","UPN",1,10,"Quimica esencial","documental",80," ",1);
        Documentales documentales2= new Documentales("Quimica","UNT",1,15,"Quimica basica","documental",90," ",1);
        Usuario usuario1= new Usuario("UNI",3,"Fisica 1",1);
        usuario1.Buscar("Fisica 1");
    }

}
