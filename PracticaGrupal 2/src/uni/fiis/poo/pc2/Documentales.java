package uni.fiis.poo.pc2;

public class Documentales extends MaterialUniversitario implements Estudiable {
    private static String titulo;
    private String genero;
    private int duracion;
    private String descripcion;
    private int capitulo;

    public Documentales(String curso,String universidad, int ciclo, double precio, String titulo, String genero, int duracion, String descripcion, int capitulo){
        super(curso, universidad, ciclo, precio);
        this.titulo=titulo;
        this.genero=genero;
        this.duracion=duracion;
        this.descripcion=descripcion;
        this.capitulo=capitulo;
    }
    public static String getTitulo() {
        return titulo;
    }
    public void mostarDatos(){
        System.out.println("Documental");
        System.out.println("Titulo: "+titulo);
        System.out.println("Capitulo: "+capitulo);
        System.out.println("Genero: "+genero);
        System.out.println("Duracion: "+duracion);
        System.out.println("Descripcion: "+descripcion);
    }

    @Override
    public int obtenerTiempoEstudio() {
        return duracion;
    }
}
