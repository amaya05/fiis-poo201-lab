package uni.fiis.poo.pc2;

public abstract class MaterialUniversitario {
    private String curso;
    private String universidad;
    private int ciclo;
    private double precio;

    public MaterialUniversitario(String curso, String universidad, int ciclo, double precio){
        this.curso=curso;
        this.universidad=universidad;
        this.ciclo=ciclo;
        this.precio=precio;
    }
    public void mostarDatos(){
        System.out.println("Curso: "+curso);
        System.out.println("Universidad: "+universidad);
        System.out.println("Ciclo: "+ciclo);
        System.out.println("Precio: "+precio);
    }


}
