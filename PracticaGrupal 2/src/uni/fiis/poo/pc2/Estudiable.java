package uni.fiis.poo.pc2;

public interface Estudiable {

    public int obtenerTiempoEstudio();
}
