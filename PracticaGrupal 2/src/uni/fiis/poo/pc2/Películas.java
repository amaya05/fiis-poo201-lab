package uni.fiis.poo.pc2;

public class Películas extends MaterialUniversitario implements Estudiable {
    private  static String titulo;
    private String genero;
    private int duracion;
    private String descripcion;

    public Películas(String curso,String universidad, int ciclo, double precio, String titulo, String genero, int duracion, String descripcion){
        super(curso, universidad, ciclo, precio);
        this.titulo=titulo;
        this.genero=genero;
        this.duracion=duracion;
        this.descripcion=descripcion;
    }

    public static String getTitulo() {
        return titulo;
    }
    public void mostarDatos(){
        System.out.println("Pelicula");
        System.out.println("Titulo: "+titulo);
        System.out.println("Genero: "+genero);
        System.out.println("Duracion: "+duracion);
        System.out.println("Descripcion: "+descripcion);
    }

    @Override
    public int obtenerTiempoEstudio() {
        return duracion;
    }
}
