package uni.fiis.poo.pc2.Parte2;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class Menu {
    public static void main(String[] args){
        ArrayList<Especializacion> l1=new ArrayList<>();
        ArrayList<Especializacion> l2=new ArrayList<>();
        GestionEmpleados lista=new GestionEmpleados();
        Especializacion c1= new Especializacion("SQL","07/07/17","25/10/17","UNI");
        Especializacion c2= new Especializacion("Python","05/03/17","28/06/17","UNI");
        l1.add(c1);
        l1.add(c2);
        Especializacion c3= new Especializacion("Finanzas","04/07/16","29/10/16","PUCP");
        Especializacion c4= new Especializacion("Excel","02/03/16","25/06/16","UNI");
        l2.add(c3);
        l2.add(c4);
        Empleado e1= new Empleado("121","75432315","Juan","Alvarez","12/03/19",3000,"Pedro Perez","TI",l1);
        Empleado e2= new Empleado("111","76534378","Raul","Castillo","15/01/19",4000,"Cesar Lujan","Logistica",l2);
        lista.empleados.add(e1);
        lista.empleados.add(e2);
        System.out.println("**** Menu ****");
        System.out.println("Seleccione la opcion a ejecutar:");
        System.out.println("1)Agregar empleado");
        System.out.println("2)Modificar datos de empleado");
        System.out.println("3)Eliminar empleado");
        System.out.println("4)Gestionar especializaciones");
        System.out.println("5)Mostrar informacion");
        Scanner sc = new Scanner(System.in);
        int resp = sc.nextInt();
        Iterator<Empleado> it=lista.empleados.iterator();
        switch (resp){
            case 1:
                System.out.println("ID:");
                String resp0 = sc.next();
                System.out.println("DNI:");
                String resp1 = sc.next();
                System.out.println("Nombre:");
                String resp2 = sc.next();
                System.out.println("Apellido:");
                String resp3 = sc.next();
                System.out.println("Fecha de contratacion:");
                String resp4 = sc.next();
                System.out.println("Salario:");
                double resp5 = sc.nextDouble();
                System.out.println("Supervisor a cargo:");
                String resp6 = sc.next();
                System.out.println("Departamento asignado:");
                String resp7 = sc.next();
                Empleado emp= new Empleado(resp0,resp1,resp2,resp3,resp4,resp5,resp6,resp7,null);
                lista.empleados.add(emp);
                break;
            case 2:
                System.out.println("ID de empleado:");
                resp0 = sc.next();
                System.out.println("Seleccione dato a modificar:");
                System.out.println("a)DNI");
                System.out.println("b)Nombre");
                System.out.println("c)Apellido");
                System.out.println("d)Fecha de contratacion");
                System.out.println("e)Salario");
                System.out.println("f)Supervisor a cargo");
                System.out.println("g)Departamento asignado");
                String rpta=sc.next();
                int cont=0;
                while (it.hasNext()){
                    String r=it.next().getId();
                    if(r.equals(resp0)){
                        break;
                    }
                    cont++;
                }
                switch (rpta){
                    case "a":
                        System.out.println("Nuevo DNI:");
                        rpta=sc.next();
                        lista.empleados.get(cont).setDni(rpta);
                        break;
                    case "b":
                        System.out.println("Nuevo Nombre:");
                        rpta=sc.next();
                        lista.empleados.get(cont).setNombre(rpta);
                        break;
                    case "c":
                        System.out.println("Nuevo Apellido:");
                        rpta=sc.next();
                        lista.empleados.get(cont).setApellido(rpta);
                        break;
                    case "d":
                        System.out.println("Nueva Fecha de contratacion:");
                        rpta=sc.next();
                        lista.empleados.get(cont).setFecha(rpta);
                        break;
                    case "e":
                        System.out.println("Nuevo Salario:");
                        resp5=sc.nextDouble();
                        lista.empleados.get(cont).setSalario(resp5);
                        break;
                    case "f":
                        System.out.println("Nuevo Supervisor a cargo:");
                        rpta=sc.next();
                        lista.empleados.get(cont).setSupervisor(rpta);
                        break;
                    case "g":
                        System.out.println("Nuevo Departamento asignado:");
                        rpta=sc.next();
                        lista.empleados.get(cont).setDepartamento(rpta);
                        break;
                }
                break;
            case 3:
                System.out.println("ID de empleado a eliminar");
                resp0 = sc.next();
                System.out.println("¿Confirmar accion?");
                System.out.println("a)Si");
                System.out.println("b)No");
                resp0 = sc.next();
                switch (resp0){
                    case "a":
                        while (it.hasNext()){
                            String r=it.next().getId();
                            if(r.equals(resp0)){
                                it.remove();
                                break;
                            }
                        }
                    case "b":
                        break;
                }
                break;
            case 4:
                System.out.println("Seleccion opcion a ejecutar");
                System.out.println("a)Agregar especializaciones de un empleado");
                System.out.println("b)Eliminar especializaciones de un empleado");
                resp0 = sc.next();
                switch (resp0){
                    case "a":
                        System.out.println("ID de empleado:");
                        resp0 = sc.next();
                        cont=0;
                        while (it.hasNext()){
                            String r=it.next().getId();
                            if(r.equals(resp0)){
                                break;
                            }
                            cont++;
                        }
                        System.out.println("Numero de especializaciones a agregar:");
                        resp = sc.nextInt();
                        ArrayList<Especializacion> esp=new ArrayList<>();
                        for (int i=1;i<=resp;i++){
                            System.out.println("Especializacion "+i+" :");
                            System.out.println("Nombre de especializacion:");
                            String resp8=sc.next();
                            System.out.println("Fecha inicio:");
                            String resp9=sc.next();
                            System.out.println("Fecha fin:");
                            String resp10=sc.next();
                            System.out.println("Universidad que la dicta:");
                            String resp11=sc.next();
                            Especializacion val=new Especializacion(resp8,resp9,resp10,resp11);
                            esp.add(val);
                        }
                        lista.empleados.get(cont).setEspec(esp);
                        break;
                    case "b":
                        System.out.println("ID de empleado:");
                        resp0 = sc.nextLine();
                        cont=0;
                        while (it.hasNext()){
                            String r=it.next().getId();
                            if(r.equals(resp0)){
                                break;
                            }
                            cont++;
                        }
                        lista.empleados.get(cont).setEspec(null);
                        break;
                }
            case 5:
                System.out.println("Seleccion opcion a ejecutar");
                System.out.println("a)Mostrar informacion de un empleado");
                System.out.println("b)Mostrar informacion de todos los empleados");
                resp0 = sc.next();
                switch (resp0){
                    case "a":
                        System.out.println("ID de empleado:");
                        resp0 = sc.next();
                        cont=0;
                        while (it.hasNext()){
                            String r=it.next().getId();
                            if(r.equals(resp0)){
                                System.out.println("ID: "+r);
                                System.out.println("DNI: "+lista.empleados.get(cont).getDni());
                                System.out.println("Nombre: "+lista.empleados.get(cont).getNombre());
                                System.out.println("Apellido: "+lista.empleados.get(cont).getApellido());
                                System.out.println("Fecha de contratacion: "+lista.empleados.get(cont).getFecha());
                                System.out.println("Salario: "+lista.empleados.get(cont).getSalario());
                                System.out.println("Supervisor a cargo: "+lista.empleados.get(cont).getSupervisor());
                                System.out.println("Departamento encargado: "+lista.empleados.get(cont).getDepartamento());
                                for(int j=0;j<lista.empleados.get(cont).getEspec().size();j++){
                                    System.out.println("Especializacion "+(j+1)+" :"+lista.empleados.get(cont).getEspec().get(j).getNombre());
                                }
                                break;
                            }
                            cont++;
                        }
                        break;
                    case "b":
                        for(int k=0;k<lista.empleados.size();k++){
                            System.out.println("Empleado "+(k+1)+" :");
                            System.out.println("ID: "+lista.empleados.get(k).getDni());
                            System.out.println("DNI: "+lista.empleados.get(k).getDni());
                            System.out.println("Nombre: "+lista.empleados.get(k).getNombre());
                            System.out.println("Apellido: "+lista.empleados.get(k).getApellido());
                            System.out.println("Fecha de contratacion: "+lista.empleados.get(k).getFecha());
                            System.out.println("Salario: "+lista.empleados.get(k).getSalario());
                            System.out.println("Supervisor a cargo: "+lista.empleados.get(k).getSupervisor());
                            System.out.println("Departamento encargado: "+lista.empleados.get(k).getDepartamento());
                            for(int j=0;j<lista.empleados.get(k).getEspec().size();j++){
                                System.out.println("Especializacion "+(j+1)+" :"+lista.empleados.get(k).getEspec().get(j).getNombre());
                            }
                        }
                        break;
                }
        }
    }
}
