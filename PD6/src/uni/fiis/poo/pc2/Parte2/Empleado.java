package uni.fiis.poo.pc2.Parte2;

import java.util.ArrayList;

public class Empleado {
    private String id;
    private String dni;
    private String nombre;
    private String apellido;
    private String fecha;
    private double salario;
    private String supervisor;
    private String departamento;
    private ArrayList<Especializacion> espec;

    public Empleado(String id,String dni,String nombre,String apellido,String fecha,double salario,String supervisor,String departamento,ArrayList<Especializacion> espec){
        this.id=id;
        this.dni=dni;
        this.nombre=nombre;
        this.apellido=apellido;
        this.fecha=fecha;
        this.salario=salario;
        this.supervisor=supervisor;
        this.departamento=departamento;
        this.espec=espec;
    }

    public String getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public double getSalario() {
        return salario;
    }

    public String getApellido() {
        return apellido;
    }

    public ArrayList<Especializacion> getEspec() {
        return espec;
    }

    public String getDepartamento() {
        return departamento;
    }

    public String getDni() {
        return dni;
    }

    public String getFecha() {
        return fecha;
    }

    public String getSupervisor() {
        return supervisor;
    }


    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    public void setSupervisor(String supervisor) {
        this.supervisor = supervisor;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public void setEspec(ArrayList<Especializacion> espec) {
        this.espec = espec;
    }
}
