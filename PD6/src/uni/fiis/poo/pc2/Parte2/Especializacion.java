package uni.fiis.poo.pc2.Parte2;

public class Especializacion {
    private String nombre;
    private String inicio;
    private String fin;
    private String universidad;

    public Especializacion(String nombre,String inicio,String fin,String universidad){
        this.nombre=nombre;
        this.inicio=inicio;
        this.fin=fin;
        this.universidad=universidad;
    }

    public String getNombre() {
        return nombre;
    }
}
