package uni.fiis.poo.pc2.Parte1;

import uni.fiis.poo.pc2.Parte1.AdministradorPilas;
import uni.fiis.poo.pc2.Parte1.Pila;

public class TestAdminPilas {
    public static void main(String[] args){
        AdministradorPilas admin = new AdministradorPilas();
        Pila p1 = admin.obtenerPila(2);
        p1.push(16);
        p1.push(18);
        p1.push(14);
        p1.push(25);
        p1.mostrarElementos();
        System.out.println(" ");
        Pila p2 = admin.obtenerPila(5);
        p2.push(1);
        p2.push(14);
        p2.push(18);
        p2.push(35);
        p2.push(10);
        p2.mostrarElementos();
        System.out.println(" ");
        Pila p3= admin.obtenerPila(8);
        p3.push(2);
        p3.push(23);
        p3.push(12);
        p3.push(7);
        p3.push(89);
        p3.push(45);
        p3.push(22);
        p3.push(31);
        p3.mostrarElementos();
        System.out.println(" ");
        Pila p4= admin.obtenerPila();
        p4.push(11);
        p4.push(13);
        p4.push(1);
        p4.push(111);
        p4.mostrarElementos();
    }
}
