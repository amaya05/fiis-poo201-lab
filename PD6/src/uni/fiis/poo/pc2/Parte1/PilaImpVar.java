package uni.fiis.poo.pc2.Parte1;

import uni.fiis.poo.pc2.Parte1.Pila;

public class PilaImpVar implements Pila {
    private Integer elemento1 = null;
    private Integer elemento2 = null;
    private Integer elemento3 = null;

    public void push(Integer item){
        if(elemento1 == null){
            elemento1 = item;
        }else if(elemento2 == null){
            elemento2 = item;
        }else if(elemento3 == null){
            elemento3 = item;
        }else{
            System.out.println("No se puede agregar " + item + ". Pila esta llena");
        }
    }

    public Integer pop(){
        Integer respuesta = null;
        if(elemento3 != null){
            respuesta = elemento3;
            elemento3 = null;
        }else if(elemento2 != null){
            respuesta = elemento2;
            elemento2 = null;
        }else if(elemento1 != null){
            respuesta = elemento1;
            elemento1 = null;
        }
        return respuesta;
    }

    public boolean estaVacia(){
        boolean resp = false;
        if(elemento1 == null){
            resp = true;
        }
        return resp;

    }

    public void mostrarElementos(){
        System.out.println(elemento3);
        System.out.println(elemento2);
        System.out.println(elemento1);
    }
}
