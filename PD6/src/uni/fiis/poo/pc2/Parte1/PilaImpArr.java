package uni.fiis.poo.pc2.Parte1;

import uni.fiis.poo.pc2.Parte1.Pila;

public class PilaImpArr implements Pila {
    public static final int MAX_ELEM = 6;
    private int[] valores;
    private int cima;
    public PilaImpArr(){
        valores = new int[MAX_ELEM];
        cima = -1;
    }
    public void push(Integer item){
        cima++;
        valores[cima] = item;
    }
    public Integer pop(){
        Integer respuesta = null;
        respuesta = valores[cima];
        cima--;
        return respuesta;
    }
    public boolean estaVacia(){
        return (cima == -1);
    }
    public void mostrarElementos(){
        for(int i = cima; i >= 0; i--){
            System.out.println(valores[i]);
        }
    }
}
