package uni.fiis.poo.pc2.Parte1;

import java.util.Iterator;
import java.util.LinkedList;

public class PilaImpLst implements Pila{
    LinkedList<Integer> lista=new LinkedList();
    private int tam=0;
    public void push(Integer item) {
        lista.addFirst(item);
        tam++;
    }

    public Integer pop() {
        Integer respuesta=null;
        respuesta=lista.removeFirst();
        tam--;
        return respuesta;
    }

    public boolean estaVacia() {
        boolean resp=false;
        if(tam==0){
            resp=true;
        }
        return resp;
    }

    public void mostrarElementos() {
        Iterator<Integer> it=lista.iterator();
        while (it.hasNext()){
            System.out.println(it.next());
        }
    }
}
