package uni.fiis.poo.pc2.Parte1;

import uni.fiis.poo.pc2.Parte1.Pila;
import uni.fiis.poo.pc2.Parte1.PilaImpVar;

public class TestPilaVar {
    public static void main(String[] args){
        Pila p = new PilaImpVar();
        System.out.println(p.estaVacia());
        p.push(12);
        p.push(35);
        p.push(48);

        while(p.estaVacia() == false){
            Integer valor = p.pop();
            System.out.println("Valor: " + valor);
        }
    }
}
