package uni.fiis.poo.pc2.Parte1;

public interface Pila {
    public void push(Integer item);
    public Integer pop();
    public boolean estaVacia();
    public void mostrarElementos();
}
