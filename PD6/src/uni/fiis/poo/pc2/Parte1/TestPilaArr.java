package uni.fiis.poo.pc2.Parte1;

import uni.fiis.poo.pc2.Parte1.Pila;
import uni.fiis.poo.pc2.Parte1.PilaImpArr;

public class TestPilaArr {
    public static void main(String[] args){
        Pila p = new PilaImpArr();
        System.out.println(p.estaVacia());
        p.push(12);
        p.push(35);
        p.push(48);
        p.push(50);
        p.push(65);
        p.push(123);
        System.out.println(p.estaVacia());
        p.mostrarElementos();

        while(p.estaVacia() == false){
            Integer valor = p.pop();
            System.out.println("Valor: " + valor);
        }
    }
}
