package uni.fiis.poo.pc2.Parte1;

public class TestPilaLst {
    public static void main(String[] args){
        Pila p=new PilaImpLst();
        System.out.println(p.estaVacia());
        p.push(1);
        p.push(2);
        p.push(3);
        System.out.println(p.estaVacia());
        while (p.estaVacia()==false){
            Integer valor=p.pop();
            System.out.println(valor);
        }
    }
}
