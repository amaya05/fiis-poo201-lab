package uni.fiis.poo.pc2.Parte1;

public class AdministradorPilas {
    public Pila obtenerPila(int elementos) {
        Pila respuesta = null;
        if (elementos <= 3) {
            respuesta = new PilaImpVar();
        }else {
            if(elementos>PilaImpArr.MAX_ELEM) {
                respuesta= new PilaImpLst();
            }
            else {
                respuesta= new PilaImpArr();
            }
        }
        return respuesta;
    }
    public Pila obtenerPila(){
        Pila respuesta= new PilaImpLst();
        return respuesta;
    }
}
