package uni.fiis.poo.pc1;

public class Libros extends ProductoUniversitario {
    private String codigo;
    private static String titulo;
    private String autor;
    private int num_pag;
    private int anio;
    private String descripcion;

    public Libros(String curso,String universidad, int ciclo, double precio, String codigo, String titulo, String autor, int num_pag, int anio, String descripcion){
        super(curso, universidad, ciclo, precio);
        this.codigo=codigo;
        this.titulo=titulo;
        this.autor=autor;
    }

    public static String getTitulo() {
        return titulo;
    }
}
