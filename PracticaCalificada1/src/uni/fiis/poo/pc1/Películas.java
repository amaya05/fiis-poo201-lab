package uni.fiis.poo.pc1;

public class Películas extends ProductoUniversitario {
    private  static String titulo;
    private String genero;
    private int duracion;
    private String descripcion;

    public Películas(String curso,String universidad, int ciclo, double precio, String titulo, String genero, int duracion, String descripcion){
        super(curso, universidad, ciclo, precio);
        this.titulo=titulo;
        this.genero=genero;
        this.duracion=duracion;
        this.descripcion=descripcion;
    }

    public static String getTitulo() {
        return titulo;
    }
}
