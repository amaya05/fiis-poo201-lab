package uni.fiis.poo.pc1;

public class Documentales extends ProductoUniversitario {
    private static String titulo;
    private String genero;
    private int duracion;
    private String descripcion;
    private int capitulo;

    public Documentales(String curso,String universidad, int ciclo, double precio, String titulo, String genero, int duracion, String descripcion, int capitulo){
        super(curso, universidad, ciclo, precio);
        this.titulo=titulo;
        this.genero=genero;
        this.duracion=duracion;
        this.descripcion=descripcion;
        this.capitulo=capitulo;
    }
    public static String getTitulo() {
        return titulo;
    }
}
