package edu.uni.poo.dirigida1;

public class TrianguloV3_modificado extends TrianguloV2_modificado{
    //Uso del "final" para proteger atributos
    private final double perimetro;
    private final double area;
    public TrianguloV3_modificado(double perimetro, double area, double lado1, double lado2, double lado3){
        setLado1(lado1);
        setLado2(lado2);
        setLado3(lado3);
        this.perimetro=perimetro;
        this.area=area;
    }
    public double calcularPerimetro(){
        return perimetro;
    }

    public double calcularArea(){
        return area;
    }

    public void obtenerTipoTriangulo(){
        if(getLado1()==getLado2() && getLado1()==getLado3()){
            System.out.print("Es un triangulo equilatero");
        }
        else{
            if(getLado1()==getLado2() || getLado2()==getLado3() || getLado1()==getLado3()){
                System.out.print("Es un triangulo isosceles");
            }
            else{
                System.out.print("Es un triangulo escaleno");
            }
        }

    }
}

