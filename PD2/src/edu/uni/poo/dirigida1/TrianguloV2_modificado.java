package edu.uni.poo.dirigida1;

public class TrianguloV2_modificado {
    private double lado1;
    private double lado2;
    private double lado3;
    //Metodos de acceso para lado1,lado2 y lado3
    public double getLado1(){
        return lado1;
    }

    public double getLado2() {
        return lado2;
    }

    public double getLado3() {
        return lado3;
    }

    public void setLado1(double lado1){
        this.lado1=lado1;
    }

    public void setLado2(double lado2) {
        this.lado2 = lado2;
    }

    public void setLado3(double lado3) {
        this.lado3 = lado3;
    }

    public double calcularPerimetro(){
        double perimetro;
        perimetro=lado1+lado2+lado3;
        return perimetro;
    }

    public double calcularArea(){
        double area,semi;
        semi=(lado1+lado2+lado3)/2;
        area=Math.sqrt(semi*(semi-lado1)*(semi-lado2)*(semi-lado3));
        return area;
    }

    public void obtenerTipoTriangulo(){
        if(lado1==lado2 && lado1==lado3){
            System.out.print("Es un triangulo equilatero");
        }
        else{
            if(lado1==lado2 || lado2==lado3 || lado1==lado3){
                System.out.print("Es un triangulo isosceles");
            }
            else{
                System.out.print("Es un triangulo escaleno");
            }
        }

    }
}
