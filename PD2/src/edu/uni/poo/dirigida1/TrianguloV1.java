package edu.uni.poo.dirigida1;
public class TrianguloV1 {

    public static double calcularPerimetro(double lado1, double lado2, double lado3){
        double perimetro;
        perimetro=lado1+lado2+lado3;
        return perimetro;
    }

    public static double calcularArea(double lado1, double lado2, double lado3){
        double area,semi;
        semi=(lado1+lado2+lado3)/2;
        area=Math.sqrt(semi*(semi-lado1)*(semi-lado2)*(semi-lado3));
        return area;
    }

    public static void obtenerTipoTriangulo(double lado1, double lado2, double lado3){
        if(lado1==lado2 && lado1==lado3){
            System.out.print("Es un triangulo equilatero");
         }
        else{
            if(lado1==lado2 || lado2==lado3 || lado1==lado3){
                System.out.print("Es un triangulo isosceles");
            }
            else{
                System.out.print("Es un triangulo escaleno");
            }
        }

     }
}
