package edu.uni.poo.dirigida1.test;

import edu.uni.poo.dirigida1.TrianguloV2;

public class PruebaTrianguloV2 {
    public static void main(String[] args) {
        //Es necesario instanciar
        TrianguloV2 triangulo1= new TrianguloV2(3,4,5);
        System.out.println("El perimetro es: "+ triangulo1.calcularPerimetro());
        System.out.println("El area es: "+ triangulo1.calcularArea());
        triangulo1.obtenerTipoTriangulo();
    }
}
