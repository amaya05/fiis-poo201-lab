package edu.uni.poo.dirigida1.test;
import edu.uni.poo.dirigida1.TrianguloV1;
public class PruebaTrianguloV1 {

    public static void main(String[] args) {
        //No es necesario instanciar
        System.out.println("El perimetro es: "+TrianguloV1.calcularPerimetro(3,3,3));
        System.out.println("El area es: "+TrianguloV1.calcularArea(3,3,3));
        TrianguloV1.obtenerTipoTriangulo(3,3,3);
    }
    
}
