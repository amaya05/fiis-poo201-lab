package edu.uni.poo.dirigida1.test;

import edu.uni.poo.dirigida1.TrianguloV3;

public class PruebaTrianguloV3 {
    public static void main(String[] args) {
        TrianguloV3 triangulo3= new TrianguloV3(24,24,6,8,10);
        System.out.println("El area es: "+triangulo3.calcularPerimetro());
        System.out.println("El area es: "+triangulo3.calcularArea());
        triangulo3.obtenerTipoTriangulo();
        triangulo3.perimetro = 12;
        triangulo3.area = 30;
        // Error!! No se puede acceder a los atributos
        //Los atributos están definidos en private
        //Para acceder a los tributos deben de estar definidos en public
    }
}
