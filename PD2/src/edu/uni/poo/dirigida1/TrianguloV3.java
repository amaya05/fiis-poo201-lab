package edu.uni.poo.dirigida1;

public class TrianguloV3 extends TrianguloV2_modificado{
     private double perimetro;
     private double area;
    // Metodo constructor
    public TrianguloV3(double perimetro, double area, double lado1, double lado2, double lado3){
        setLado1(lado1);
        setLado2(lado2);
        setLado3(lado3);
        this.perimetro=perimetro;
        this.area=area;
    }
    public double calcularPerimetro(){
        return perimetro;
    }

    public double calcularArea(){
        return area;
    }

    public void obtenerTipoTriangulo(){
        if(getLado1()==getLado2() && getLado1()==getLado3()){
            System.out.print("Es un triangulo equilatero");
        }
        else{
            if(getLado1()==getLado2() || getLado2()==getLado3() || getLado1()==getLado3()){
                System.out.print("Es un triangulo isosceles");
            }
            else{
                System.out.print("Es un triangulo escaleno");
            }
        }

    }
}
