package uni.fiis.poo.ep;

import java.util.ArrayList;

public class Paciente {
    private String nombre;
    private String apellido;
    private String dni;
    private String telefono;
    private String direccion;
    private ArrayList<String> padecimientos;
    public Paciente(String nombre,String apellido,String dni,String telefono,String direccion,ArrayList<String> padecimientos){
        this.nombre=nombre;
        this.apellido=apellido;
        this.dni=dni;
        this.telefono=telefono;
        this.direccion=direccion;
        this.padecimientos=padecimientos;
    }
}
