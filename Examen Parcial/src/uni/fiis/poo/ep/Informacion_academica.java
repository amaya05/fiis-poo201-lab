package uni.fiis.poo.ep;

public class Informacion_academica{
    private String univ_pre;
    private String univ_esp;
    private String maestria;
    private String doctorado;
    public Informacion_academica(String univ_pre,String univ_esp,String maestria,String doctorado){
        this.univ_pre=univ_pre;
        this.univ_esp=univ_esp;
        this.maestria=maestria;
        this.doctorado=doctorado;
    }
}
