package uni.fiis.poo.ep;

public class Medico {
    private String nombre;
    private String apellido;
    private String dni;
    private String telefono;
    private String direccion;
    private String num_cole_med;
    private String res_exp;
    private Informacion_academica info;
    private String sede;
    private String especialidad;

    public  Medico(String nombre,String apellido,String dni,String telefono,String direccion,String num_cole_med,String res_exp,Informacion_academica info,String sede,String especialidad){
        this.nombre=nombre;
        this.apellido=apellido;
        this.dni=dni;
        this.telefono=telefono;
        this.direccion=direccion;
        this.num_cole_med=num_cole_med;
        this.res_exp=res_exp;
        this.info=info;
        this.sede=sede;
        this.especialidad=especialidad;
    }
}
