package uni.fiis.poo.pc2;

import java.util.ArrayList;

public class Usuario {
    private String nombre;
    private String dni;
    private String telefono;
    private ArrayList<String> direccion;

    public Usuario(String nombre, String dni, String telefono, ArrayList<String> direccion){
        this.nombre=nombre;
        this.dni=dni;
        this.telefono=telefono;
        this.direccion=direccion;
    }
}
