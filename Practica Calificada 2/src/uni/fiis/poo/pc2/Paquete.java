package uni.fiis.poo.pc2;

public class Paquete {
    private String codigo;
    private Producto producto;
    private String nombre_dest;
    private Coordenada origen;
    private Coordenada destino;
    private String fecha_recep;
    private String fecha_entrega;
    private String estado;

    public Paquete(String codigo,Producto producto,String nombre_dest,Coordenada origen,Coordenada destino,String fecha_recep,String fecha_entrega,String estado){
        this.codigo=codigo;
        this.producto=producto;
        this.nombre_dest=nombre_dest;
        this.origen=origen;
        this.destino=destino;
        this.fecha_entrega=fecha_entrega;
        this.fecha_recep=fecha_recep;
        this.estado=estado;
    }
}
