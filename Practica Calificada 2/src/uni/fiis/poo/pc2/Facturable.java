package uni.fiis.poo.pc2;

public interface Facturable {
    public double obtenerTarifa();
}
