package uni.fiis.poo.pc2;

public class Motorizado {
    private String nombre;
    private String dni;
    private String telefono;
    private String direccion;
    private double salario;
    private String distrito;
    private String provincia;

    public Motorizado(String nombre,String dni,String telefono,String direccion,double salario,String distrito,String provincia){
        this.nombre=nombre;
        this.dni=dni;
        this.telefono=telefono;
        this.direccion=direccion;
        this.salario=salario;
        this.distrito=distrito;
        this.provincia=provincia;
    }
}
