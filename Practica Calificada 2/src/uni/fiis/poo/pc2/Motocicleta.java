package uni.fiis.poo.pc2;

public class Motocicleta {
    private String placa;
    private String marca;
    private String modelo;
    private double velocidad;
    private String num_poliza;
    private boolean disponible;

    public Motocicleta(String placa,String marca,String modelo,double velocidad,String num_poliza,boolean disponible){
        this.placa=placa;
        this.marca=marca;
        this.modelo=modelo;
        this.velocidad=velocidad;
        this.num_poliza=num_poliza;
        this.disponible=disponible;
    }
}
