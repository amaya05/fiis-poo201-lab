package uni.fiis.poo.pc2;

public class Cuadrado extends Poligono {
    VisualizadorPoligono vistaPoligono;
    public Cuadrado(){
        super(4);
    }
    @Override
    public double calcularPerimetro(){
        double longitud=Math.sqrt(Math.pow((vertices.get(0).abcisa-vertices.get(1).abcisa),2)+Math.pow((vertices.get(0).ordenada-vertices.get(1).ordenada),2));
        double  perimetro=4*longitud;
        return perimetro;
    }
    @Override
    public double calcularArea(){
        double longitud=Math.sqrt(Math.pow((vertices.get(0).abcisa-vertices.get(1).abcisa),2)+Math.pow((vertices.get(0).ordenada-vertices.get(1).ordenada),2));
        double  area=Math.pow(longitud,2);
        return area;
    }
    @Override
    public void dibujar() {
        VisualizadorPoligono visualizadorPoligono = new VisualizadorPoligono(this.vertices,"Cuadrado");
        vistaPoligono=visualizadorPoligono;
        vistaPoligono.mostrar();
    }
    @Override
    public void dibujar(int coef) {
        VisualizadorPoligono visualizadorPoligono = new VisualizadorPoligono(this.vertices,"Cuadrado");
        vistaPoligono=visualizadorPoligono;
        vistaPoligono.setSize(coef);
        vistaPoligono.mostrar();
    }

}
