package uni.fiis.poo.pc2;
import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class VisualizadorPoligono extends JPanel {
    int[] coordenadasx;
    int[] coordenadasy;
    String titulo;
    private double size=1.0;
    JFrame frame;
    dibujodelpoligono panel;
    public VisualizadorPoligono(ArrayList<Punto> coords, String title){
        int [] coordsx =new int[coords.size()];
        int [] coordsy =new int[coords.size()];
        for(int i=0; i<coords.size();i++){
            coordsx[i]=coords.get(i).abcisa;
            coordsy[i]=coords.get(i).ordenada;
        }
        coordenadasx=coordsx;
        coordenadasy=coordsy;
        titulo=title;
        frame = new JFrame(titulo);
        panel = new dibujodelpoligono();
    }
    public void mostrar(){
        JFrame.setDefaultLookAndFeelDecorated(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setBackground(Color.white);
        frame.setSize((int)(Math.round(300*size)), (int)(Math.round(300*size)));
        panel.coordesx=coordenadasx;
        panel.coordesy=coordenadasy;
        frame.add(panel);
        frame.setVisible(true);
    }

    public void setSize(double size) {
        this.size = size;
    }
}
