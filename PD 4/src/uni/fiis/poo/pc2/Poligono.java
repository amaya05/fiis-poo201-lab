package uni.fiis.poo.pc2;

import java.util.ArrayList;
import java.lang.Math;

public abstract class Poligono implements Dibujable {
    ArrayList<Punto> vertices= new ArrayList();
    protected int n_lados;

    public Poligono(int lados) {
        n_lados=lados;
    }
    public void agregarVertice(Punto punto){
        Punto n_punto=punto;
        vertices.add(n_punto);

    }
    public void mostrarDatos(){
        System.out.printf("El polígono tiene %d lados\n",n_lados);
        for (int i=0; i<vertices.size();i++){
            System.out.printf("Punto %d: (%d,%d)\n",i+1,vertices.get(i).abcisa,vertices.get(i).ordenada);;
            if ((i+1)<n_lados){
                double longitud=Math.sqrt(Math.pow((vertices.get(i).abcisa-vertices.get(i+1).abcisa),2)+Math.pow((vertices.get(i).ordenada-vertices.get(i+1).ordenada),2));
                System.out.printf("Lado %d tiene una longitud de %f\n",i+1,longitud);
            }
            else {
                double longitud=Math.sqrt(Math.pow((vertices.get(i).abcisa-vertices.get(0).abcisa),2)+Math.pow((vertices.get(i).ordenada-vertices.get(0).ordenada),2));
                System.out.printf("Lado %d tiene una longitud de %f\n",i+1,longitud);
            }
        }
    }
    public boolean validar(){
        if (vertices.size()==(n_lados)){
            return true;
        } else {
            return false;
        }
    }

    public void setN_lados(int n_lados) {
        this.n_lados = n_lados;
    }
    public abstract double calcularPerimetro();
    public abstract double calcularArea();
}
