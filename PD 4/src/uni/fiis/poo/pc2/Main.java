package uni.fiis.poo.pc2;

public class Main {
    public static void main(String[] args) {
        Cuadrado cuadradin = new Cuadrado();

        Punto punto4 = new Punto();
        Punto punto5 = new Punto();
        Punto punto6 = new Punto();
        Punto punto7 = new Punto();

        punto4.abcisa=10;
        punto4.ordenada=10;
        punto5.abcisa=10;
        punto5.ordenada=210;
        punto6.abcisa=210;
        punto6.ordenada=210;
        punto7.abcisa=210;
        punto7.ordenada=10;
        cuadradin.agregarVertice(punto4);
        cuadradin.agregarVertice(punto5);
        cuadradin.agregarVertice(punto6);
        cuadradin.agregarVertice(punto7);
        cuadradin.mostrarDatos();
        System.out.println("El perimetro del cuadrado es:");
        System.out.println(cuadradin.calcularPerimetro());
        System.out.println("El area del cuadrado es:");
        System.out.println(cuadradin.calcularArea());
        System.out.printf("Validando...\n");
        System.out.println(cuadradin.validar());
        System.out.printf("Dibujando con tamaño de panel 300x300\n");
        cuadradin.dibujar();
        System.out.printf("Dibujando multiplicando el tamaño de los lados por 3\n");
        cuadradin.dibujar(3);

        Triangulo triangulin = new Triangulo();

        Punto punto1 = new Punto();
        Punto punto2 = new Punto();
        Punto punto3 = new Punto();
        punto1.abcisa=10;
        punto1.ordenada=10;
        punto2.abcisa=100;
        punto2.ordenada=150;
        punto3.abcisa=190;
        punto3.ordenada=10;
        triangulin.agregarVertice(punto1);
        triangulin.agregarVertice(punto2);
        triangulin.agregarVertice(punto3);
        triangulin.mostrarDatos();
        System.out.println("El perimetro del triangulo es:");
        System.out.println(triangulin.calcularPerimetro());
        System.out.println("El area del triangulo es:");
        System.out.println(triangulin.calcularArea());
        System.out.printf("Validando...\n");
        System.out.println(triangulin.validar());
        triangulin.setN_lados(4);
        System.out.printf("Validando habiendo cambiado el número de lados...\n");
        System.out.println(triangulin.validar());
        triangulin.setN_lados(3);
        System.out.printf("Dibujando con tamaño de panel 300x300\n");
        triangulin.dibujar();
        System.out.printf("Dibujando multiplicando el tamaño de los lados por 2\n");
        triangulin.dibujar(2);


    }

}
