package uni.fiis.poo.pc2;

public interface  Dibujable{
    public void dibujar();
    public void dibujar(int coef);
}
