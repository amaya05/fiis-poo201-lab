package uni.fiis.poo.pc2;
public class Triangulo extends Poligono {
    VisualizadorPoligono vistaPoligono;
    public Triangulo(){
        super(3);
    }
    @Override
    public double calcularPerimetro(){
        double perimetro=0;
        for (int i=0; i<vertices.size();i++){
            if ((i+1)<n_lados){
                double longitud=Math.sqrt(Math.pow((vertices.get(i).abcisa-vertices.get(i+1).abcisa),2)+Math.pow((vertices.get(i).ordenada-vertices.get(i+1).ordenada),2));
                perimetro+=longitud;
            }
            else {
                double longitud=Math.sqrt(Math.pow((vertices.get(i).abcisa-vertices.get(0).abcisa),2)+Math.pow((vertices.get(i).ordenada-vertices.get(0).ordenada),2));
                perimetro+=longitud;
            }
        }
        return perimetro;
    }
    @Override
    public double calcularArea(){
        double[] longitud=new double[3];
        double p=calcularPerimetro()/2;
        double area;
        for (int i=0; i<vertices.size();i++){
            if ((i+1)<n_lados){
                 longitud[i]=Math.sqrt(Math.pow((vertices.get(i).abcisa-vertices.get(i+1).abcisa),2)+Math.pow((vertices.get(i).ordenada-vertices.get(i+1).ordenada),2));
            }
            else {
                longitud[2]=Math.sqrt(Math.pow((vertices.get(i).abcisa-vertices.get(0).abcisa),2)+Math.pow((vertices.get(i).ordenada-vertices.get(0).ordenada),2));
            }
        }
        area=Math.sqrt(p*(p-longitud[0])*(p-longitud[1])*(p-longitud[2]));
        return area;
    }
    @Override
    public void dibujar() {
        VisualizadorPoligono visualizadorPoligono = new VisualizadorPoligono(this.vertices,"Triangulo");
        vistaPoligono=visualizadorPoligono;
        vistaPoligono.mostrar();
    }
    @Override
    public void dibujar(int coef) {
        VisualizadorPoligono visualizadorPoligono = new VisualizadorPoligono(this.vertices,"Triangulo");
        vistaPoligono=visualizadorPoligono;
        vistaPoligono.setSize(coef);
        vistaPoligono.mostrar();
    }

}
